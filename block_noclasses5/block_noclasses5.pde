//various global variables
boolean start = true;
float ballX;
float ballY;
float ballspeed=4;
float xspeed =4;
float yspeed =4;
int startTimer =0;
float [] blockX1values = new float [10];
float [] blockY1values = new float [10];
float [] blockX2values = new float [40];
float [] blockY2values = new float [40];
float [] blockX3values = new float [30];
float [] blockY3values = new float [30];
int blocksdestroyed = 0;
int lives = 2;
float paddlelength= 75;
float relativeIntersectX;
float normalizedRelativeIntersectX;
float bounceAngle;
float MaxBounceAngle = ((5*PI) / 12);
int levelSelect=0;
String levSelect = "Press 1 to 9 for desired difficulty level";
boolean level1start = false;
boolean level2start = false;
boolean level3start = false;

float bulletX = mouseX;
float bulletY = 642.5;
boolean bullet = false;

void setup () {
  background(125);
  size(750, 750);

  // initialize level 1 block array
  for (int i=0; i<blockX1values.length; i+=1) {
    blockX1values[i] = 37.5;
    blockY1values[i] = 12.5;
  }
  //initilize level 2 block array
  for (int i=0; i<blockX2values.length; i+=1) {
    blockX2values[i] = 37.5;
    blockY2values[i] = 12.5;
  }
}



void draw () {
  background(125);
  // at start of game, press 1 to 9 for different levels. 
  if (levelSelect == 0) {
    textSize(36);
    text(levSelect, (width/2)-325, height/2);
  } else if (levelSelect != 0) {
    text(levSelect, 1000, 1000);
  }
//choose from the various difficult levels
  if (levelSelect == 0) {
    if (key == '1') {
      levelSelect = 1;
      level1start = true;
    } else if ( key == '2') {
      levelSelect = 2;
      level2start = true;
    } else if ( key == '3') {
      levelSelect = 3;
      level3start = true;
    } else if ( key == '4') {
      levelSelect = 4;
    } else if ( key == '5') {
      levelSelect = 5;
    } else if ( key == '6') {
      levelSelect = 6;
    } else if ( key == '7') {
      levelSelect = 7;
    } else if ( key == '8') {
      levelSelect = 8;
    } else if ( key == '9') {
      levelSelect = 9;
    }
  }
//choose the different levels (theres only 2 created at the moment!
  if (levelSelect == 1) {
    if (level1start==true) {
      blockX1values();
    }
  } else if (levelSelect == 2) {
    if (level2start == true) {
      blockX2values();
      blockY2values();
    }
  } else if (levelSelect == 3) {
    if (level3start == true) {
      blockX3values();
      blockY3values();
    }
  }


  rectMode(CENTER);
  //draw the paddle
  fill(0);
  rect(mouseX, 650, paddlelength, 15);

  //draw the ball
  if (start==true) {
    ballX = mouseX;
    ballY = 635;
    fill(255);
    ellipse(ballX, ballY, 15, 15);
  } else if (start==false) {
    fill(255);
    ellipse(ballX, ballY, 15, 15);
  }

  //if (key == 's' || key == 'S') {
  //  start = false;
  //}
  //if (start == false) {
  //  if (key == 's' || key == 'S') {
  //    start= true;
  //  } } else if(start == true) {
  //    if (key == 's' || key == 'S') {
  //      start = false;
  //    }
  //  }


  //ballX and ballY change values according to the value of xspeed and yspeed, respectively
  ballX += xspeed;
  ballY -= yspeed;


  //timer once started
  if (start== false) {
    startTimer +=1;
    println("number blocks destroyed:" + blocksdestroyed);
    println("level selected is:" + levelSelect);
    println("bullet:"+bullet);
    //println("number of lives:" + lives);

    // if start = false, set levelstart to false
    level1start=false;
    level2start=false;
    level3start=false;
  } else if (start == true) {
    println("bullet:"+bullet);
  }

  //bounce on walls
  wallBounce();
  ceilingBounce ();
  paddleBounce ();

  //initiate and drawblocks
  if (levelSelect == 1) {
    for (int i =0; i<blockX1values.length; i+=1) {
      fill(random(0, 255), random(0, 255), random(0, 255));
      rect(blockX1values[i], blockY1values[i], 75, 25);
    }
  } else if (levelSelect == 2) {
    for (int i =0; i<blockX2values.length; i+=1) {
      fill(random(0, 255), random(0, 255), random(0, 255));
      rect(blockX2values[i], blockY2values[i], 75, 25);
    }
  } else if (levelSelect == 3) {
    for (int i =0; i<blockX3values.length; i+=1) {
      fill(random(0, 255), random(0, 255), random(0, 255));
      rect(blockX3values[i], blockY3values[i], 75, 25);
    }
  }

  ////calculate distance between ball and blocks 
  if (levelSelect == 1) {
    for (int i=0; i<blockX1values.length; i+=1) {
      if (dist(ballX, (ballY+7.5), blockX1values[i], blockY1values[i]) < 39.53) {
        yspeed*= -1;
        blocksdestroyed +=1;
        blockY1values[i] = 1000;
      }
    }
  } else if (levelSelect == 2) {
    for (int j=0; j<blockX2values.length; j+=1) {
      if (dist(ballX, (ballY+7.5), blockX2values[j], blockY2values[j]) < 39.53) {
        yspeed*= -1;
        blocksdestroyed +=1;
        blockY2values[j] = 1000;
      }
    }
  } else if (levelSelect == 3) {
    for (int j=0; j<blockX3values.length; j+=1) {
      if (dist(ballX, (ballY+7.5), blockX3values[j], blockY3values[j]) < 39.53) {
        yspeed*= -1;
        blocksdestroyed +=1;
        blockY3values[j] = 1000;
      }
    }
  }

  //endgame condition
  if (levelSelect == 1) {
    if (blocksdestroyed >= 10) {
      background(125);
      fill(0);
      String endLevelMessage = "YOU BEAT THE LEVEL!";
      String endLevelMessage2 = "Press 'l' for next level";
      text(endLevelMessage, (width/2)-170, height/2);
      text(endLevelMessage2, (width/2)-160, (height/2) + 100);
    }
    if (key == 'l') {
      level2start=true;
      start= true;
      startTimer = 0;
      xspeed = 0;
      yspeed = 0;
      blocksdestroyed=0;
      levelSelect = 2;
    }
  } else if (levelSelect == 2) {
    if (blocksdestroyed == 40) {
      background(125);
      fill(0);
      String endLevelMessage = "YOU BEAT THE LEVEL!";
      String endLevelMessage2 = "Press 'l' for next level";
      text(endLevelMessage, (width/2)-170, height/2);
      text(endLevelMessage2, (width/2)-160, (height/2) + 100); 
      if (key == 'l') {
        level3start=true;
        start = true;
        startTimer = 0;
        xspeed = 0;
        yspeed = 0;
        blocksdestroyed=0;
        levelSelect = 3;
      }
    }
  } else if (levelSelect == 3) {
    if (blocksdestroyed == 30) {
      background(125);
      fill(0);
      String endLevelMessage = "YOU BEAT THE LEVEL!";
      String endLevelMessage2 = "Press 'l' for next level";
      text(endLevelMessage, (width/2)-170, height/2);
      text(endLevelMessage2, (width/2)-160, (height/2) + 100); 
      if (key == 'l') {
        start = true;
        startTimer = 0;
        xspeed = 0;
        yspeed = 0;
        blocksdestroyed=0;
        levelSelect = 4;
      }
    }
  }




  //losinglives
  if (ballY >= height-30) { //&& ballY<= 702) {
    lives = lives-1;
    start=true;
    startTimer = 0;
    xspeed = 0;
    yspeed = 0;
  }

  //black bar of death down the bottom
  fill(95, 139, 224);
  for (int i = 10; i< width; i+=20) {
    stroke(255);
    triangle(i, height-20, i+10, height-20, i+5, height-30);
  }
  //noStroke();
  //fill(125);
  //ellipse(11,height-23,10,7);

  fill(0);
  rect(width/2, height-10, width, 20);
  //lives display
  if (lives >= 3) {
    fill(255);
    ellipse(715, 745, 10, 10);
    ellipse(730, 745, 10, 10);
    ellipse(745, 745, 10, 10);
  } else if (lives == 2) {
    fill(255);
    ellipse(715, 745, 10, 10);
    ellipse(730, 745, 10, 10);
  } else if (lives == 1) {
    fill(255);
    ellipse (715, 745, 10, 10);
  } else if (lives == 0) {
  }

  if (lives < 0) {
    background(125);
    String endmessage = "Better luck next time, buddy!";
    textSize(48);
    text(endmessage, width/4, height/2);
  }

  // A FEW TOOLS USEFUL FOR DEBUGGING
  //println ("number of lives:"+lives);
  //println ("relativeIntersectX:"+ normalizedRelativeIntersectX);
  //for (int i= 0; i< blockY2values.length ; i+=1) {
  //  print("blockY2value"+"["+i+"]is:"+blockY2values[i]);
  //}
  //}

  //bullet
  if (bullet == false) {
    bulletX = mouseX;
    bulletY = 642.5;
  } else if (bullet == true) {
    ellipse(bulletX, bulletY, 5, 5);
    bulletY-=8;
  }
  if (key == 'b') {
    bullet = true;
  }
 if (bulletY <= 0) {
   bullet = false;
 }
}
//if mouse is clicked..
void mouseClicked () {
  start = false;

  if (level1start == true) {
    level1start=false;
  }

  if (level2start == true) {
    level2start=false;
  }

  if (startTimer == 0) {
    xspeed = 2;
    yspeed = 2;
  }
}

//void mousePressed () {
//  if (start == false && startTimer >= 10) {
//  bulletY -= 1;
//}
//}

void wallBounce () {
  if ((ballX-7.5)<= 0 || (ballX+7.5) >= width) {
    xspeed *= -1;
  }
}

void ceilingBounce () {
  if ((ballY-7.5) <= 0) {
    yspeed *= -1;
  }
}

void paddleBounce () {
  //bounce on top of paddle
  if (start== false && startTimer >= 10) {
    if (ballY <= 636 && ballY >=635) {   
      if ((ballX) >= (mouseX-37.5) && (ballX) <= (mouseX+37.5)) {
        relativeIntersectX = mouseX-ballX;
        normalizedRelativeIntersectX = (relativeIntersectX / (paddlelength/2));
        bounceAngle = normalizedRelativeIntersectX * MaxBounceAngle;
        xspeed = ballspeed*-sin(bounceAngle);
        yspeed = ballspeed*cos(bounceAngle);
      }
    }
    // //bounce side of paddle
    if ((ballY+7.5) <= 642 && (ballY-7.5) >= 658) {
      if ((ballX+7.5) >= (mouseX-37.5) || (ballX-7.5) <= (mouseX+37.5)) {
        yspeed = 2;
        xspeed*=-1;
      }
    }
  }
}

void blockX1values () {
  for (int i=1; i < blockX1values.length; i+=1) {
    blockX1values[i]= blockX1values[i-1] +75;
  }
}

void blockX2values () {
  for (int i=1; i <= 9; i+=1) {
    blockX2values[i] = blockX2values[i-1] + 75;
  } 
  for (int i = 11; i <= 19; i+=1) {
    blockX2values[i] = blockX2values[i-1] + 75;
  } 
  for (int i = 21; i <= 29; i+=1) {
    blockX2values[i] = blockX2values[i-1] + 75;
  } 
  for (int i = 31; i <= 39; i+=1) {
    blockX2values[i] = blockX2values[i-1] + 75;
  }
}

void blockY2values () {
  for (int i = 10; i <= 19; i+=1) {
    blockY2values[i] = 37.5;
  } 
  for (int i = 20; i <= 29; i+=1) {
    blockY2values[i] = 62.5;
  } 
  for (int i = 30; i <= 39; i+=1) {
    blockY2values[i] = 87.5;
  }
}

void blockX3values () {

  for (int i = 0; i<= 4; i+=1) {
    blockX3values [i] = 37.5;
  }
  for (int i = 5; i<= 8; i+=1) {
    blockX3values [i] = 112.5;
  }
  for (int i = 9; i<= 11; i+=1) {
    blockX3values [i] = 187.5;
  }
  for (int i = 12; i<= 13; i+=1) {
    blockX3values [i] = 262.5;
  }

  blockX3values[14] = 337.5;

  blockX3values[15] = 412.5;

  for (int i = 16; i<= 17; i+=1) {
    blockX3values [i] = 487.5;
  }
  for (int i = 18; i<= 20; i+=1) {
    blockX3values [i] = 562.5;
  }
  for (int i = 21; i<= 24; i+=1) {
    blockX3values [i] = 637.5;
  }
  for (int i = 25; i<= 29; i+=1) {
    blockX3values [i] = 712.5;
  }
}

void blockY3values () {
  blockY3values [0] = 37.5;
  blockY3values [1] = 112.5;
  blockY3values [2] = 187.5;
  blockY3values [3] = 262.5;
  blockY3values [4] = 337.5;
  blockY3values [5] = 37.5;
  blockY3values [6] = 112.5;
  blockY3values [7] = 187.5;
  blockY3values [8] = 262.5;
  blockY3values [9] = 37.5;
  blockY3values [10] = 112.5;
  blockY3values [11] = 187.5;
  blockY3values [12] = 37.5;
  blockY3values [13] = 112.5;
  blockY3values [14] = 37.5;
  blockY3values [15] = 37.5;
  blockY3values [16] = 37.5;
  blockY3values [17] = 112.5;
  blockY3values [18] = 37.5;
  blockY3values [19] = 112.5;
  blockY3values [20] = 187.5;
  blockY3values [21] = 37.5;
  blockY3values [22] = 112.5;
  blockY3values [23] = 187.5;
  blockY3values [24] = 262.5;
  blockY3values [25] = 37.5;
  blockY3values [26] = 112.5;
  blockY3values [27] = 187.5;
  blockY3values [28] = 262.5;
  blockY3values [29] = 337.5;
}